FROM andreroquem/ocaml-build

COPY --chown=opam:opam . /home/opam/app

RUN oasis setup
RUN ocaml setup.ml -configure --enable-tests
RUN ocaml setup.ml -build
RUN ocaml setup.ml -test